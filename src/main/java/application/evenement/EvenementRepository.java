/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.evenement;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author matthieu
 */
public interface EvenementRepository extends CrudRepository<Evenement, Integer> {
    
}
