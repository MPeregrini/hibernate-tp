/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.evenement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author matthieu
 */
@Controller
@RequestMapping(value="/evenements")
public class EvenementController {
    
    @Autowired
    private EvenementRepository evenementRepository;
    
    @GetMapping(value="/liste")
    public String listeEvenements(Model model) {
        model.addAttribute("evenements", evenementRepository.findAll());
        return "evenements/listeEvenements";
    }
    
    @GetMapping(value="/nouveau")
    public String nouvelEvenement(Model model) {
        model.addAttribute("evenement", new Evenement());
        return "evenements/nouvelEvenement";
    }
    
    @PostMapping(value="/nouveau")
    public String ajoutNouvelEvenement(@ModelAttribute("Evenement") Evenement evenement) {
        evenementRepository.save(evenement);
        return "redirect:/evenements/liste";
    }
}
