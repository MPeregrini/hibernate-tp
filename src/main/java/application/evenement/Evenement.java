/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.evenement;

import application.participant.Participant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author matthieu
 */

@Entity
@Table(name = "Evenement")
public class Evenement {
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    @Column(name = "numeroEvenement")
    private int numeroEvenement;
    
    @Column(name = "intitule")
    private String intitule;
    
    @Column(name = "theme")
    private String theme;
    
    @Column(name = "dateDebut")
    private String dateDebut;
    
    @Column(name = "duree")
    private String duree;
    
    @Column(name = "nbParticipantMax")
    private int nbParticipantMax;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "organisateur")
    private String organisateur;
    /*
    @OneToMany(mappedBy = "evenement")
    private List<Participant> participants;
    */
    public Evenement() {}
    
    public int getNumeroEvenement() {
        return numeroEvenement;
    }

    public void setNumeroEvenement(int numeroEvenement) {
        this.numeroEvenement = numeroEvenement;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public int getNbParticipantMax() {
        return nbParticipantMax;
    }

    public void setNbParticipantMax(int nbParticipantMax) {
        this.nbParticipantMax = nbParticipantMax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }
    /*
    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
    */    
}
