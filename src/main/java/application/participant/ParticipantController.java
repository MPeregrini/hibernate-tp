/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.participant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author matthieu
 */
@Controller
@RequestMapping(value="/participants")
public class ParticipantController {
    
    @Autowired
    private ParticipantRepository participantRepository;
    
    @GetMapping(value="/liste")
    public String listeParticipants(Model model) {
        model.addAttribute("participants", participantRepository.findAll());
        return "participants/listeParticipants";
    }
    
    @GetMapping(value="/nouveau")
    public String nouveauParticipant(Model model) {
        model.addAttribute("participant", new Participant());
        return "participants/nouveauParticipant";
    }
    
    @PostMapping(value="/nouveau")
    public String ajoutNouveauParticipant(@ModelAttribute("Participant") Participant participant) {
        participantRepository.save(participant);
        return "redirect:/participants/liste";
    }
}
